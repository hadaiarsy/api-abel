<?php

use Illuminate\Database\Seeder;
use App\Notifikasi;

class NotifikasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = Notifikasi::all();
        if (count($check) <= 0) {
            $notif = new Notifikasi;
            $notif->judul_notif = "Pembelajaran PHP";
            $notif->isi_notif = "Minngu depan akan diadakan pra-test untuk mengukur sejauh mana kalian mengetahui tentang bahasa pemrograman PHP. Oleh karena itu dimohon semua anak agar mempersiapkan dirinya sebaik mungkin untuk nanti. Terima kasih :)";
            $notif->user_id = 2;
            $notif->kelas_id = 1;
            $notif->image = "woman-1.jpg";
            $notif->is_active = 1;
            $notif->save();
        }
    }
}
