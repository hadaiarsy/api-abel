<?php

use Illuminate\Database\Seeder;
use App\Nilai;

class NilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = count(Nilai::all());
        if ($check <= 0) {
            DB::table("nilai")->insert(array(
                array(
                    "guru_id" => "G-19001",
                    "mapel_id" => 1,
                    "nilai_murid" => 80,
                    "loop_nilai" => 1,
                    "murid_id" => "192010001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                )
            ));
        }        
    }
}
