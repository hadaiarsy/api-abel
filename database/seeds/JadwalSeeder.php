<?php

use Illuminate\Database\Seeder;
use App\Jadwal;
class JadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = count(Jadwal::all());
        if ($check <= 0) {
            DB::table("jadwal")->insert(array(
                array(
                    "waktu_awal" => "07:45",
                    "waktu_akhir" => "08:30",
                    "hari" => "Rabu",
                    "mapel_id" => 1,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "08:30",
                    "waktu_akhir" => "09:15",
                    "hari" => "Rabu",
                    "mapel_id" => 1,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "09:15",
                    "waktu_akhir" => "10:00",
                    "hari" => "Rabu",
                    "mapel_id" => 1,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "10:30",
                    "waktu_akhir" => "11:15",
                    "hari" => "Rabu",
                    "mapel_id" => 1,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "12:15",
                    "waktu_akhir" => "13:00",
                    "hari" => "Rabu",
                    "mapel_id" => 2,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "13:00",
                    "waktu_akhir" => "13:45",
                    "hari" => "Rabu",
                    "mapel_id" => 2,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "13:45",
                    "waktu_akhir" => "14:30",
                    "hari" => "Rabu",
                    "mapel_id" => 3,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "14:30",
                    "waktu_akhir" => "15:15",
                    "hari" => "Rabu",
                    "mapel_id" => 3,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "15:15",
                    "waktu_akhir" => "16:00",
                    "hari" => "Rabu",
                    "mapel_id" => 4,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ),
                array(
                    "waktu_awal" => "16:00",
                    "waktu_akhir" => "16:45",
                    "hari" => "Rabu",
                    "mapel_id" => 4,
                    "ruang" => 30,
                    "kelas_id" => 1,
                    "guru_id" => "G-19001",
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                )
            ));
        }
    }
}
