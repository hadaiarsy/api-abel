<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;
use App\Guru;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create("id_ID");

        for ($i = 1; $i <= 3; ++$i) {
            $guru = new Guru;
            $guru->nip_guru = 0000000000000000;
            $guru->nama_guru = $faker->firstName . ' ' . $faker->lastName;
            $guru->alamat_guru = $faker->address;
            $guru->no_tlp_guru = $faker->phoneNumber;
            $guru->mapel_id = 1;
            $guru->password = strtoupper(Str::random(6));
            $guru->phone_id = '';
            $guru->save();
        }
    }
}
