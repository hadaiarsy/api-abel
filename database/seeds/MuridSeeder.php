<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;
use App\Murid;

class MuridSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create("id_ID");

        for ($i = 1; $i <= 650; ++$i) {
            $murid = new Murid;
            $murid->nama_murid = $faker->firstName . ' ' . $faker->lastName;
            $murid->alamat_murid = $faker->address;
            $murid->no_tlp_murid = $faker->phoneNumber;
            $murid->kelas_id = 1;
            $murid->is_registered = "false";
            $murid->save();
        }
    }
}
