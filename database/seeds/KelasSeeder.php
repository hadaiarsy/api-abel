<?php

use Illuminate\Database\Seeder;
use App\Kelas;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = count(Kelas::all());
        if ($check <= 0) {
            $kelas = new Kelas;
            $kelas->nama_kelas = "XII-RPL-1";
            $kelas->wali_kelas_id = "G-19001";
            $kelas->save();
        }
    }
}
