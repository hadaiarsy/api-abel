<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(GuruSeeder::class);
        $this->call(MuridSeeder::class);
        $this->call(KelasSeeder::class);
        $this->call(MapelSeeder::class);
        $this->call(JadwalSeeder::class);
        $this->call(NilaiSeeder::class);
        $this->call(NotifikasiSeeder::class);
    }
}
