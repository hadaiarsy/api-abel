<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = count(User::all());
        if ($check <= 0) {
            $user = new User;
            $user->name = "admin";
            $user->email = "admin.abel@gmail.com";
            $user->email_verified_at = now();
            $user->password = Hash::make('password');
            $user->remember_token = Str::random(16);
            $user->save();
        }        
    }
}
