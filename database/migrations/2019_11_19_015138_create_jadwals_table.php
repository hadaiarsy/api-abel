<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('waktu_awal')->nullable();
            $table->string('waktu_akhir')->nullable();
            $table->timestamp('tanggal')->nullable();
            $table->string('hari')->nullable();
            $table->integer('mapel_id')->nullable();
            $table->integer('ruang')->nullable();
            $table->integer('kelas_id')->nullable();
            $table->string('guru_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal');
    }
}
