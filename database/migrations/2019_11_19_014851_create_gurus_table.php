<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guru', function (Blueprint $table) {
            $table->char('kode_guru', 7)->primary();
            $table->string('nip_guru')->nullable();
            $table->string('nama_guru');
            $table->text('alamat_guru');
            $table->string('no_tlp_guru');
            $table->integer('mapel_id');
            $table->string('password')->nullable();
            $table->string('phone_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guru');
    }
}
