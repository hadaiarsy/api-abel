<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthApiController@login')->name('login');
    Route::post('register', 'Api\AuthApiController@register');
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\AuthApiController@logout');
        Route::get('user', 'Api\AuthApiController@user');
    });
});

Route::group([], function() {

    // auth
    // Route::post('/auth/login', 'Api\AuthApiController@login');
    // Route::post('/auth/register', 'Api\AuthApiController@register');
    // Route::post('/auth/signup', 'Api\AuthApiController@signup');
    // Route::get('/auth/logout', 'Api\AuthApiController@logout');
    // Route::get('/auth/user', 'Api\AuthApiController@user');

    // jadwal
    Route::get('/jadwal/{kelas}/{hari}', 'Api\JadwalApiController@get');
    Route::get('/jadwal/guru/{guru}/{hari}', 'Api\JadwalApiController@getGuru');

    // notifikasi
    Route::get('/notifikasi', 'Api\NotifikasiApiController@get');
    Route::get('/notifikasi/guru/{guru}', 'Api\NotifikasiApiController@guru');
    Route::get('/notifikasi/show/{notif_id}', 'Api\NotifikasiApiController@show');
    Route::get('/notifikasi/create', 'Api\NotifikasiApiController@create');
    Route::post('/notifikasi', 'Api\NotifikasiApiController@store');
    Route::get('/notifikasi/{id}/edit', 'Api\NotifikasiApiController@edit');
    Route::patch('/notifikasi/{id}', 'Api\NotifikasiApiController@update');
    Route::delete('/notifikasi/{id}', 'Api\NotifikasiApiController@destroy');
});
