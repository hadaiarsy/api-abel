<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    Route::resource('guru', 'GuruPageController');
    Route::resource('murid', 'MuridPageController');
    Route::resource('kelas', 'KelasPageController');
    Route::resource('mapel', 'MapelPageController');
    Route::resource('jadwal', 'JadwalPageController');
    Route::get('/jadwal/see/{kelas_id}/{hari}', 'JadwalPageController@see');

    Route::get('/notifikasi', 'NotifikasiPageController@index');
    Route::get('/notifikasi-create', 'NotifikasiPageController@createNotif');
    Route::get('/notifikasi/{id}', 'NotifikasiPageController@show');
    Route::get('/notifikasi/active/{id}', 'NotifikasiPageController@active');
    Route::post('/notifikasi', 'NotifikasiPageController@store');
    Route::delete('/notifikasi/{id}', 'NotifikasiPageController@destroy');
});
