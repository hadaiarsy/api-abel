@extends('admin.layouts.main')
@section('submenu', 'Manajemen Sekolah')
@section('title', 'Kelas')

@section('content')

@foreach($mapel as $m)
<!-- form edit -->
<h4>Edit Data</h4>
<hr>
<form action="/mapel/{{ $m->id }}" method="POST">
@csrf
@method('patch')

  <div class="form-group">
    <label for="nama_mapel">Nama Mapel</label>
    <input type="text" class="form-control @error('nama_mapel') is-invalid @enderror" name="nama_mapel" id="nama_mapel" value="{{ $m->nama_mapel }}" placeholder="Enter Nama Mapel">
    @error('nama_mapel')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <button type="submit" class="mb-2 btn btn-primary">Submit</button>
</form>
<!-- endform -->
<hr>
<!-- form hapus -->
<form action="/mapel/{{ $m->id }}" method="post" class="mt-3 mb-5">
@csrf
@method('delete')
    <button type="submit" class="btn btn-danger">Hapus Data</button>
</form>
<!-- endform -->
@endforeach

@endsection