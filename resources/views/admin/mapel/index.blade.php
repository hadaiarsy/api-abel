@extends('admin.layouts.main')
@section('submenu', 'Manajemen Sekolah')
@section('title', 'Mapel')

@section('content')

<!-- col -->
<div class="col-sm-12 col-lg-12">
    <div class="card text-white bg-flat-color-1">
        <div class="card-body pb-0">
            <div class="dropdown float-right">
                <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
                    <i class="fa fa-cog"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <div class="dropdown-menu-content">
                        <a class="dropdown-item" href="/mapel/create">Create New</a>
                    </div>
                </div>
            </div>
            
            <h4 class="mb-0">
                <span class="count">{{ count($mapel) }}</span>
            </h4>
            <p class="text-light">Mapel</p>

        </div>

    </div>
</div>
<!--/.col-->

<!-- table -->
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Mapel</th>
      <th scope="col">SET</th>
    </tr>
  </thead>
  <tbody>
    @foreach($mapel as $m)
    <tr>
      <th scope="row">{{ $loop->iteration }}</th>
      <td>{{ $m->nama_mapel }}</td>
      <td>
        <a href="/mapel/{{ $m->id }}/edit" class="badge badge-warning">Edit</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<!-- endtable -->

@endsection