@extends('admin.layouts.main')
@section('submenu', 'Manajemen Sekolah')
@section('title', 'Kelas')

@section('content')

<!-- form -->
<form action="/mapel" method="POST">
@csrf

  <div class="form-group">
    <label for="nama_mapel">Nama Kelas</label>
    <input type="text" class="form-control @error('nama_mapel') is-invalid @enderror" name="nama_mapel" id="nama_mapel" value="{{ old('nama_mapel') }}" placeholder="Enter Nama Mapel">
    @error('nama_mapel')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <button type="submit" class="mb-4 btn btn-primary">Submit</button>
</form>
<!-- endform -->

@endsection