@extends('admin.layouts.main')
@section('submenu', 'Manajemen Data')
@section('title', 'Guru')

@section('content')

@foreach($guru as $g)
<!-- form edit -->
<h4>Edit Data</h4>
<hr>
<form action="/guru/{{ $g->kode_guru }}" method="POST">
@csrf
@method('patch')

  <div class="form-group">
    <label for="nip_guru">NIP</label>
    <input type="text" class="form-control @error('nip_guru') is-invalid @enderror" name="nip_guru" id="nip_guru" value="{{ $g->nip_guru }}" placeholder="Enter NIP">
    @error('nip_guru')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="nama_guru">Nama</label>
    <input type="text" class="form-control @error('nama_guru') is-invalid @enderror" name="nama_guru" id="nama_guru" value="{{ $g->nama_guru }}" placeholder="Enter Nama">
    @error('nama_guru')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="alamat_guru">Alamat</label>
    <textarea class="form-control @error('alamat_guru') is-invalid @enderror" name="alamat_guru" id="alamat_guru" rows="3">{{ $g->alamat_guru }}</textarea>
    @error('alamat_guru')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="no_tlp_guru">No. TLP</label>
    <input type="text" class="form-control @error('no_tlp_guru') is-invalid @enderror" name="no_tlp_guru" id="no_tlp_guru"value="{{ $g->no_tlp_guru }}" placeholder="Enter No. TLP">
    @error('no_tlp_guru')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="mapel_id">Mapel</label>
    <select class="form-control @error('mapel_id') is-invalid @enderror" name="mapel_id" id="mapel_id">
    @foreach($mapel as $m)
    @if ($g->mapel->id == $m->id)
      <option value="{{ $m->id }}" selected>{{ $m->nama_mapel }}</option>
    @else
      <option value="{{ $m->id }}">{{ $m->nama_mapel }}</option>
    @endif
    @endforeach
    </select>
    @error('mapel_id')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="text" class="form-control @error('password') is-invalid @enderror" name="password" id="password" value="{{ $g->password }}" placeholder="Enter Password">
    @error('password')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="phone_id">Phone ID</label>
    <input type="text" class="form-control" name="phone_id" id="phone_id"value="{{ $g->phone_id }}" placeholder="Enter Phone ID">
  </div>
  <button type="submit" class="mb-2 btn btn-primary">Submit</button>
</form>
<!-- endform -->
<hr>
<!-- form hapus -->
<form action="/guru/{{ $g->kode_guru }}" method="post" class="mt-3 mb-5">
@csrf
@method('delete')
    <button type="submit" class="btn btn-danger">Hapus Data</button>
</form>
<!-- endform -->
@endforeach

@endsection