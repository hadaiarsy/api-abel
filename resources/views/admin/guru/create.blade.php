@extends('admin.layouts.main')
@section('submenu', 'Manajemen Data')
@section('title', 'Guru')

@section('content')

<!-- form -->
<form action="/guru" method="POST">
@csrf

  <div class="form-group">
    <label for="nip_guru">NIP</label>
    <input type="text" class="form-control @error('nip_guru') is-invalid @enderror" name="nip_guru" id="nip_guru" value="{{ old('nip_guru') }}" placeholder="Enter NIP">
    @error('nip_guru')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="nama_guru">Nama</label>
    <input type="text" class="form-control @error('nama_guru') is-invalid @enderror" name="nama_guru" id="nama_guru" value="{{ old('nama_guru') }}" placeholder="Enter Nama">
    @error('nama_guru')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="alamat_guru">Alamat</label>
    <textarea class="form-control @error('alamat_guru') is-invalid @enderror" name="alamat_guru" id="alamat_guru" rows="3">{{ old('alamat_guru') }}</textarea>
    @error('alamat_guru')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="no_tlp_guru">No. TLP</label>
    <input type="text" class="form-control @error('no_tlp_guru') is-invalid @enderror" name="no_tlp_guru" id="no_tlp_guru" value="{{ old('no_tlp_guru') }}" placeholder="Enter No. TLP">
    @error('no_tlp_guru')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="mapel_id">Mapel</label>
    <select class="form-control @error('mapel_id') is-invalid @enderror" name="mapel_id" id="mapel_id">
    @foreach($mapel as $m)
      <option value="{{ $m->id }}">{{ $m->nama_mapel }}</option>
    @endforeach
    </select>
    @error('mapel_id')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <button type="submit" class="mb-4 btn btn-primary">Submit</button>
</form>
<!-- endform -->

@endsection