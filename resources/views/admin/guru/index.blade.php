@extends('admin.layouts.main')
@section('submenu', 'Manajemen Data')
@section('title', 'Guru')

@section('content')

<!-- col -->
<div class="col-sm-12 col-lg-12">
    <div class="card text-white bg-flat-color-1">
        <div class="card-body pb-0">
            <div class="dropdown float-right">
                <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
                    <i class="fa fa-cog"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <div class="dropdown-menu-content">
                        <a class="dropdown-item" href="/guru/create">Create New</a>
                    </div>
                </div>
            </div>
            
            <h4 class="mb-0">
                <span class="count">{{ count($guru) }}</span>
            </h4>
            <p class="text-light">Guru</p>

        </div>

    </div>
</div>
<!--/.col-->

<!-- table -->
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Kode Guru</th>
      <th scope="col">NIP</th>
      <th scope="col">Nama</th>
      <th scope="col">Mapel</th>
      <th scope="col">Alamat</th>
      <th scope="col">No. TLP</th>
      <th scope="col">Password</th>
      <th scope="col">Phone ID</th>
      <th scope="col">SET</th>
    </tr>
  </thead>
  <tbody>
    @foreach($guru as $g)
    <tr>
      <th scope="row">{{ $loop->iteration }}</th>
      <td>{{ $g->kode_guru }}</td>
      <td>{{ $g->nip_guru }}</td>
      <td>{{ $g->nama_guru }}</td>
      <td>{{ $g->mapel->nama_mapel }}</td>
      <td>{{ $g->alamat_guru }}</td>
      <td>{{ $g->no_tlp_guru }}</td>
      <td>{{ $g->password }}</td>
      <td>{{ $g->phone_id }}</td>
      <td>
        <a href="/guru/{{ $g->kode_guru }}/edit" class="badge badge-warning">Edit</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<!-- endtable -->

@endsection