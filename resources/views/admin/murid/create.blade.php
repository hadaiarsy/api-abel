@extends('admin.layouts.main')
@section('submenu', 'Manajemen Data')
@section('title', 'Murid')

@section('content')

<!-- form -->
<form action="/murid" method="POST">
@csrf

  <div class="form-group">
    <label for="nama_murid">Nama</label>
    <input type="text" class="form-control @error('nama_murid') is-invalid @enderror" name="nama_murid" id="nama_murid" value="{{ old('nama_murid') }}" placeholder="Enter Nama">
    @error('nama_murid')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="kelas_id">Kelas</label>
    <select class="form-control @error('kelas_id') is-invalid @enderror" name="kelas_id" id="kelas_id">
    @foreach($kelas as $k)
      <option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
    @endforeach
    </select>
    @error('kelas_id')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="alamat_murid">Alamat</label>
    <textarea class="form-control @error('alamat_murid') is-invalid @enderror" name="alamat_murid" id="alamat_murid" rows="3">{{ old('alamat_murid') }}</textarea>
    @error('alamat_murid')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="no_tlp_murid">No. TLP</label>
    <input type="text" class="form-control @error('no_tlp_murid') is-invalid @enderror" name="no_tlp_murid" id="no_tlp_murid" value="{{ old('no_tlp_murid') }}" placeholder="Enter No. TLP">
    @error('no_tlp_murid')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <button type="submit" class="mb-4 btn btn-primary">Submit</button>
</form>
<!-- endform -->

@endsection