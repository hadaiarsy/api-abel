@extends('admin.layouts.main')
@section('submenu', 'Manajemen Data')
@section('title', 'Guru')

@section('content')

@foreach($murid as $m)
<!-- form edit -->
<h4>Edit Data</h4>
<hr>
<form action="/murid/{{ $m->nis }}" method="POST">
@csrf
@method('patch')

  <div class="form-group">
    <label for="nama_murid">Nama</label>
    <input type="text" class="form-control @error('nama_murid') is-invalid @enderror" name="nama_murid" id="nama_murid" value="{{ $m->nama_murid }}" placeholder="Enter Nama">
    @error('nama_murid')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="kelas_id">Kelas</label>
    <select class="form-control @error('kelas_id') is-invalid @enderror" name="kelas_id" id="kelas_id">
    @foreach($kelas as $k)
    @if ($m->kelas->id == $k->id)
      <option value="{{ $k->id }}" selected>{{ $k->nama_kelas }}</option>
    @else
      <option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
    @endif
    @endforeach
    </select>
    @error('kelas_id')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="alamat_murid">Alamat</label>
    <textarea class="form-control @error('alamat_murid') is-invalid @enderror" name="alamat_murid" id="alamat_murid" rows="3">{{ $m->alamat_murid }}</textarea>
    @error('alamat_murid')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="no_tlp_murid">No. TLP</label>
    <input type="text" class="form-control @error('no_tlp_murid') is-invalid @enderror" name="no_tlp_murid" id="no_tlp_murid"value="{{ $m->no_tlp_murid }}" placeholder="Enter No. TLP">
    @error('no_tlp_murid')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <button type="submit" class="mb-2 btn btn-primary">Submit</button>
</form>
<!-- endform -->
<hr>
<!-- form hapus -->
<form action="/murid/{{ $m->nis }}" method="post" class="mt-3 mb-5">
@csrf
@method('delete')
    <button type="submit" class="btn btn-danger">Hapus Data</button>
</form>
<!-- endform -->
@endforeach

@endsection