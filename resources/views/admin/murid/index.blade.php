@extends('admin.layouts.main')
@section('submenu', 'Manajemen Data')
@section('title', 'Murid')

@section('content')

<!-- col -->
<div class="col-sm-12 col-lg-12">
    <div class="card text-white bg-flat-color-1">
        <div class="card-body pb-0">
            <div class="dropdown float-right">
                <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
                    <i class="fa fa-cog"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <div class="dropdown-menu-content">
                        <a class="dropdown-item" href="/murid/create">Create New</a>
                    </div>
                </div>
            </div>
            
            <h4 class="mb-0">
                <span class="count">{{ count($murid) }}</span>
            </h4>
            <p class="text-light">Murid</p>

        </div>

    </div>
</div>
<!--/.col-->

<!-- table -->
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">NIS</th>
      <th scope="col">IS REGISTERED</th>
      <th scope="col">Nama</th>
      <th scope="col">Kelas</th>
      <th scope="col">Alamat</th>
      <th scope="col">No. TLP</th>
      <th scope="col">SET</th>
    </tr>
  </thead>
  <tbody>
    @foreach($murid as $m)
    <tr>
      <th scope="row">{{ $loop->iteration }}</th>
      <td>{{ $m->nis }}</td>
      <td>{{ $m->is_registered }}</td>
      <td>{{ $m->nama_murid }}</td>
      <td>{{ $m->kelas->nama_kelas }}</td>
      <td>{{ $m->alamat_murid }}</td>
      <td>{{ $m->no_tlp_murid }}</td>
      <td>
        <a href="/murid/{{ $m->nis }}/edit" class="badge badge-warning">Edit</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<!-- endtable -->

@endsection