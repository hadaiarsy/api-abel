@extends('admin.layouts.main')
@section('submenu', 'Manajemen Jadwal')
@section('title', 'Jadwal')

@section('content')

<form action="/jadwal" method="get">
    <div class="form-group">
        <label for="kelas_id">Kelas</label>
        <select class="form-control @error('kelas_id') is-invalid @enderror" name="kelas_id" id="kelas_id">
        @foreach($kelas as $k)
            @if ($k->id == $id)
            <option value="{{ $k->id }}" selected>{{ $k->nama_kelas }}</option>
            @else
            <option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
            @endif  
        @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="hari">Hari</label>
        <select class="form-control @error('hari') is-invalid @enderror" name="hari" id="hari">
            <option value="Senin" <?= ($hari == "Senin") ? "Selected" : "" ?>>Senin</option>
            <option value="Selasa"  <?= ($hari == "Selasa") ? "Selected" : "" ?>>Selasa</option>
            <option value="Rabu" <?= ($hari == "Rabu") ? "Selected" : "" ?>>Rabu</option>
            <option value="Kamis" <?= ($hari == "Kamis") ? "Selected" : "" ?>>Kamis</option>
            <option value="Jumat" <?= ($hari == "Jumat") ? "Selected" : "" ?>>Jumat</option>
        </select>
    </div>
    <button type="submit" class="btn btn-success">Submit</button>
</form>

<hr class="mt-3 mb-4">

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Jam</th>
      <th scope="col">Mapel</th>
      <th scope="col">Ruang</th>
      <th scope="col">Guru</th>
    </tr>
  </thead>
  <tbody>
@foreach($jadwal as $j)
    <tr>
      <th scope="row">{{ $loop->iteration }}</th>
      <td>{{ $j->waktu_awal }} - {{ $j->waktu_akhir }}</td>
      <td>{{ $j->mapel->nama_mapel }}</td>
      <td>{{ $j->ruang }}</td>
      <td>{{ $j->guru->nama_guru }}</td>
    </tr>
@endforeach
  </tbody>
</table>


@endsection