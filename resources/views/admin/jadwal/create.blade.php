@extends('admin.layouts.main')
@section('submenu', 'Manajemen Jadwal')
@section('title', 'Jadwal')

@section('content')

<form action="/jadwal" method="post">
@csrf
    <div class="form-group">
        <label for="kelas_id">Kelas</label>
        <select class="form-control @error('kelas_id') is-invalid @enderror" name="kelas_id" id="kelas_id">
        @foreach($kelas as $k)
            <option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
        @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="hari">Hari</label>
        <select class="form-control @error('hari') is-invalid @enderror" name="hari" id="hari">
            <option value="Senin">Senin</option>
            <option value="Selasa">Selasa</option>
            <option value="Rabu">Rabu</option>
            <option value="Kamis">Kamis</option>
            <option value="Jumat">Jumat</option>
        </select>
    </div>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Jam</th>
                <th scope="col">Mapel</th>
                <th scope="col">Ruang</th>
                <th scope="col">Guru</th>
            </tr>
        </thead>
        <tbody>
            @for($i = 1; $i <= 12; ++$i)
            <tr>
                <td>
                    <div class="form-group d-inline">
                        <input type="time" class="form-control d-inline" name="waktu_awal[]" id="waktu_awal">
                        <input type="time" class="form-control d-inline" name="waktu_akhir[]" id="waktu_akhir">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <select class="form-control @error('mapel_id') is-invalid @enderror" name="mapel_id[]" id="mapel_id">
                        @foreach($mapel as $m)
                            <option value="{{ $m->id }}">{{ $m->nama_mapel }}</option>
                        @endforeach
                        </select>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" class="form-control" name="ruang[]" id="ruang">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <select class="form-control @error('guru_id') is-invalid @enderror" name="guru_id[]" id="guru_id">
                        @foreach($guru as $g)
                            <option value="{{ $g->kode_guru }}">{{ $g->nama_guru }}</option>
                        @endforeach
                        </select>
                    </div>
                </td>
            </tr>
            @endfor
        </tbody>
    </table>
    <button type="submit" class="btn btn-success mt-3 mb-4">Submit</button>
</form>

@endsection