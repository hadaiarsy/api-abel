@extends('admin.layouts.main')
@section('submenu', 'Manajemen Sekolah')
@section('title', 'Kelas')

@section('content')

<!-- form -->
<form action="/kelas" method="POST">
@csrf

  <div class="form-group">
    <label for="nama_kelas">Nama Kelas</label>
    <input type="text" class="form-control @error('nama_kelas') is-invalid @enderror" name="nama_kelas" id="nama_kelas" value="{{ old('nama_kelas') }}" placeholder="Enter Nama Kelas">
    @error('nama_kelas')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="wali_kelas_id">Wali Kelas</label>
    <select class="form-control @error('wali_kelas_id') is-invalid @enderror" name="wali_kelas_id" id="wali_kelas_id">
    @foreach($wali_kelas as $k)
      <option value="{{ $k->kode_guru }}">{{ $k->nama_guru }}</option>
    @endforeach
    </select>
    @error('wali_kelas_id')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <button type="submit" class="mb-4 btn btn-primary">Submit</button>
</form>
<!-- endform -->

@endsection