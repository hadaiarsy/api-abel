@extends('admin.layouts.main')
@section('submenu', 'Manajemen Sekolah')
@section('title', 'Kelas')

@section('content')

@foreach($kelas as $k)
<!-- form edit -->
<h4>Edit Data</h4>
<hr>
<form action="/kelas/{{ $k->id }}" method="POST">
@csrf
@method('patch')

  <div class="form-group">
    <label for="nama_kelas">Nama Kelas</label>
    <input type="text" class="form-control @error('nama_kelas') is-invalid @enderror" name="nama_kelas" id="nama_kelas" value="{{ $k->nama_kelas }}" placeholder="Enter Nama Kelas">
    @error('nama_kelas')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="wali_kelas_id">Wali Kelas</label>
    <select class="form-control @error('wali_kelas_id') is-invalid @enderror" name="wali_kelas_id" id="wali_kelas_id">
    @foreach($wali_kelas as $g)
    @if ($g->kode_guru == $k->wali_kelas_id)
      <option value="{{ $g->kode_guru }}" selected>{{ $g->nama_guru }}</option>
    @else
      <option value="{{ $g->kode_guru }}">{{ $g->nama_guru }}</option>
    @endif
    @endforeach
    </select>
    @error('wali_kelas_id')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <button type="submit" class="mb-2 btn btn-primary">Submit</button>
</form>
<!-- endform -->
<hr>
<!-- form hapus -->
<form action="/kelas/{{ $k->id }}" method="post" class="mt-3 mb-5">
@csrf
@method('delete')
    <button type="submit" class="btn btn-danger">Hapus Data</button>
</form>
<!-- endform -->
@endforeach

@endsection