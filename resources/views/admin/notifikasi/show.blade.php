@extends('admin.layouts.main')
@section('submenu', 'Manajemen Notifikasi')
@section('title', 'Notifikasi')

@section('content')

@foreach($notif as $n)
<div class="card text-center">
  <div class="card-header">
    {{ $n->judul_notif }}
  </div>
  <div class="card-body">
    <h5 class="card-title">Content</h5>
    <p class="small">From : {{ $n->guru->nama_guru }}</p>
    <p class="small">To : {{ $n->kelas->nama_kelas }}</p>
    <p class="card-text">{{ $n->isi_notif }}</p>
    <a href="/notifikasi" class="btn btn-primary">BACK</a>
    <form class="d-inline" action="/notifikasi/{{ $n->id }}" method="POST">
        @csrf
        @method('delete')
        <button type="submit" class="btn btn-danger">DELETE</button>
    </form>
  </div>
  <div class="card-footer text-muted">
    {{ $n->created_at }}
  </div>
</div>
@endforeach

@endsection