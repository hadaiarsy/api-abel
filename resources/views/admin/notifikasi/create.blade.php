@extends('admin.layouts.main')
@section('submenu', 'Manajemen Notifikasi')
@section('title', 'Create')

@section('content')

<!-- form -->
<form action="/notifikasi" method="POST">
@csrf

  <div class="form-group">
    <label for="judul_notif">Judul</label>
    <input type="text" class="form-control @error('judul_notif') is-invalid @enderror" name="judul_notif" id="judul_notif" value="{{ old('judul_notif') }}" placeholder="Enter Nama">
    @error('judul_notif')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="isi_notif">Konten</label>
    <textarea class="form-control @error('isi_notif') is-invalid @enderror" name="isi_notif" id="isi_notif" rows="3">{{ old('isi_notif') }}</textarea>
    @error('isi_notif')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="guru_id">Guru</label>
    <select class="form-control @error('guru_id') is-invalid @enderror" name="guru_id" id="guru_id">
    @foreach($guru as $g)
      <option value="{{ $g->kode_guru }}">{{ $g->nama_guru }}</option>
    @endforeach
    </select>
    @error('guru_id')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="kelas_id">Kelas</label>
    <select class="form-control @error('kelas_id') is-invalid @enderror" name="kelas_id" id="kelas_id">
    @foreach($kelas as $k)
      <option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
    @endforeach
    </select>
    @error('kelas_id')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
  </div>
  <div class="form-group">
    <label for="is_active">Status</label>
    <select class="form-control @error('is_active') is-invalid @enderror" name="is_active" id="is_active">
      <option value="0">Nonactive</option>
      <option value="1">Active</option>
    </select>
  </div>
  <button type="submit" class="mb-4 btn btn-primary">Submit</button>
</form>
<!-- endform -->

@endsection