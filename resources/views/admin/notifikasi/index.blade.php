@extends('admin.layouts.main')
@section('submenu', 'Manajemen Notifikasi')
@section('title', 'Notifikasi')

@section('content')

<!-- col -->
<div class="col-sm-12 col-lg-12">
    <div class="card text-white bg-flat-color-1">
        <div class="card-body pb-0">
            <div class="dropdown float-right">
                <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
                    <i class="fa fa-cog"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <div class="dropdown-menu-content">
                        <a class="dropdown-item" href="/notifikasi-create">Create New</a>
                    </div>
                </div>
            </div>
            
            <h4 class="mb-0">
                <span class="count">{{ count($notif) }}</span>
            </h4>
            <p class="text-light">Notifikasi</p>

        </div>

    </div>
</div>
<!--/.col-->

<!-- table -->
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Judul</th>
      <th scope="col">Kelas</th>
      <th scope="col">Guru</th>
      <th scope="col">Active</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @foreach($notif as $n)
    <tr>
      <th scope="row">{{ $loop->iteration }}</th>
      <td>{{ $n->judul_notif }}</td>
      <td>{{ $n->kelas->nama_kelas }}</td>
      <td>{{ $n->guru->nama_guru }}</td>
      <td>
        <a href="/notifikasi/active/{{ $n->id }}" class="badge {{ ($n->is_active == 1) ? 'badge-success' : 'badge-secondary'}}">{{ ($n->is_active == 1) ? 'active' : 'non-active' }}</a>
      </td>
      <td>
        <a href="/notifikasi/{{ $n->id }}" class="badge badge-warning">show</a>
      </td>
    </tr>
@endforeach
  </tbody>
</table>

@endsection