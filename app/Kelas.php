<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kelas extends Model
{
    use SoftDeletes;

    protected $table = "kelas";
    protected $fillable = ["nama_kelas", "wali_kelas_id"];
    protected $dates = ["deleted_at"];

    public function waliKelas()
    {
        return $this->belongsTo("App\Guru", "wali_kelas_id", "kode_guru");
    }
}
