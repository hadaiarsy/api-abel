<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifikasi;
use App\Kelas;
use App\User;

class NotifikasiApiController extends Controller
{
    public function get()
    {
        try {
            $notif = Notifikasi::with(['user', 'kelas'])->where(['is_active' => 1])->get();
            return response()->json([
                'message' => 'SUKSES',
                'serve' => $notif
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function guru($guru)
    {
        try {
            $notif = Notifikasi::with(['guru', 'kelas'])->where('guru_id', $guru)->get();
            return response()->json([
                'message' => 'SUKSES',
                'serve' => $notif
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function show($notif_id)
    {
        try {
            $notif = Notifikasi::with(['user', 'kelas'])->where(['id' => $notif_id])->get();
            return response()->json([
                'message' => 'SUKSES',
                'serve' => $notif
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function create()
    {
        try {
            $kelas = Kelas::all();
            return response()->json([
                'message' => 'SUKSES',
                'serve' => $kelas
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            Notifikasi::create($request->all());
            return response()->json([
                'message' => 'SUKSES'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function edit($id)
    {
        try {
            $notif = Notifikasi::where('id', $id)->get();
            return response()->json([
                'message' => 'SUKSES',
                'serve' => $notif
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            Notifikasi::where('id', $id)->update([
                'judul_notif' => $request->judul_notif,
                'isi_notif' => $request->isi_notif,
                'kelas_id' => $request->kelas_id,
                'is_active' => 0
            ]);
            return response()->json([
                'message' => 'SUKSES'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            Notifikasi::find($id)->delete();
            return response()->json([
                'message' => 'SUKSES'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
