<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jadwal;
use App\Kelas;

class JadwalApiController extends Controller
{
    public function get($kelas, $hari)
    {
        try {
            $jadwal = Jadwal::with(['guru', 'kelas', 'mapel'])->where(['kelas_id' => $kelas, 'hari' => $hari])->get();
            $kelas = Kelas::all();
            return response()->json([
                'message' => 'SUKSES',
                'serve' => ['jadwal'=>$jadwal, 'kelas'=>$kelas]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function getGuru($guru, $hari)
    {
        try {
            $jadwal = Jadwal::with(['guru', 'kelas', 'mapel'])->where(['guru_id' => $guru, 'hari' => $hari])->get();
            return response()->json([
                'message' => 'SUKSES',
                'serve' => $jadwal
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
