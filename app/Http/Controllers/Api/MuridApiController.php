<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Murid;

class MuridApiController extends Controller
{
    public function login(Request $request)
    {
        try {
            $nis_murid = Murid::find($request->nis);
            if ($nis_murid) {
                $password = Murid::where('nis', $request->nis)->get();
                foreach ($password as $p) {
                    $password = $p->password;
                }
                if ($request->password == $password) {
                    Murid::find($nis_murid)->update([
                        'phone_id' => $request->phone_id
                    ]);
                    $murid = Murid::with('kelas')->where('nis', $nis_murid)->get();
                    return rsponse()->json([
                        'message' => 'SUSKSES',
                        'serve' => $murid
                    ]);
                } else {
                    return 'gagal';
                }
            } else {
                return 'gagal';
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function logout(Request $request)
    {
        try {
            Murid::find($request->nis)->update([
                'phone_id' => ''
            ]);
            return 'logout';
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
