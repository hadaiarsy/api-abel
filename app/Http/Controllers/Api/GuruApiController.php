<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Guru;

class GuruApiController extends Controller
{
    public function login(Request $request)
    {
        try {
            $kode_guru = Guru::find($request->kode_guru);
            if ($kode_guru) {
                $password = Guru::where('kode_guru', $request->kode_guru)->get();
                foreach ($password as $p) {
                    $password = $p->password;
                }
                if ($request->password == $password) {
                    Guru::find($kode_guru)->update([
                        'phone_id' => $request->phone_id
                    ]);
                    $guru = Guru::with('mapel')->where('kode_guru', $kode_guru)->get();
                    return response()->json([
                        'message' => 'SUKSES',
                        'serve' => $guru
                    ]);
                } else {
                    return 'gagal';
                }
            } else {
                return 'gagal';
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function logout(Request $request)
    {
        try {
            Guru::find($request->kode_guru)->update([
                'phone_id' => ''
            ]);
            return 'logout';
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
