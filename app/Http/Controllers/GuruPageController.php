<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Guru;
use App\Mapel;

class GuruPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guru = Guru::with('mapel')->get();
        return view('admin.guru.index', [
            'guru' => $guru
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mapel = Mapel::all();
        return view('admin.guru.create', [
            'mapel' => $mapel
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nip_guru' => 'required|size:16',
            'nama_guru' => 'required',
            'alamat_guru' => 'required',
            'no_tlp_guru' => 'required',
            'mapel_id' => 'required'
        ]);

        Guru::create([
            'nip_guru' => $request->nip_guru,
            'nama_guru' => $request->nama_guru,
            'nama_guru' => $request->nama_guru,
            'alamat_guru' => $request->alamat_guru,
            'no_tlp_guru' => $request->no_tlp_guru,
            'mapel_id' => $request->mapel_id,
            'password' => strtoupper(Str::random(6))
        ]);
        return redirect('/guru')->with('status', 'Data Berhasil Masuk.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guru = Guru::with('mapel')->where('kode_guru', $id)->get();
        $mapel = Mapel::all();
        return view('admin.guru.edit', [
            'guru' => $guru,
            'mapel' => $mapel
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nip_guru' => 'required|size:16',
            'nama_guru' => 'required',
            'alamat_guru' => 'required',
            'no_tlp_guru' => 'required',
            'mapel_id' => 'required',
            'password' => 'required|min:6'
        ]);

        Guru::where('kode_guru', $id)
            ->update([
                'nip_guru' => $request->nip_guru,
                'nama_guru' => $request->nama_guru,
                'alamat_guru' => $request->alamat_guru,
                'no_tlp_guru' => $request->no_tlp_guru,
                'mapel_id' => $request->mapel_id,
                'password' => $request->password,
                'phone_id' => $request->phone_id
        ]);
        return redirect('/guru')->with('status', 'Data berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Guru::find($id)->delete();
        return redirect('/guru')->with('status', 'Data berhasil dihapus.');
    }
}
