<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;
use App\Mapel;
use App\Guru;
use App\Kelas;

class JadwalPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = (!isset($_GET['kelas_id'])) ? 1 : $_GET['kelas_id'];
        $hari = (!isset($_GET['hari'])) ? 'Senin' : $_GET['hari'];
        return redirect('/jadwal/see/' . $kelas . '/' . $hari);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Kelas::all();
        $guru = Guru::all();
        $mapel = Mapel::all();
        return view('admin.jadwal.create', [
            'kelas' => $kelas,
            'guru' => $guru,
            'mapel' => $mapel
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        for ($i = 0; $i <= (count($request->waktu_awal) - 1); ++$i) {
            $jadwal = new Jadwal;
            $jadwal->waktu_awal = $request->waktu_awal[$i];
            $jadwal->waktu_akhir = $request->waktu_akhir[$i];
            $jadwal->hari = $request->hari;
            $jadwal->mapel_id = $request->mapel_id[$i];
            $jadwal->ruang = $request->ruang[$i];
            $jadwal->kelas_id = $request->kelas_id;
            $jadwal->guru_id = $request->guru_id[$i];
            $jadwal->save();
        };
        return redirect('/jadwal')->with('status', 'Data berhasil masuk.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function see($kelas_id, $hari)
    {
        $jadwal = Jadwal::with(['mapel', 'kelas', 'guru'])->where(['kelas_id' => $kelas_id, 'hari' => $hari], 'and')->where('waktu_awal', '!=', NULL)->get();
        $kelas = Kelas::all();
        return view('admin.jadwal.show', [
            'jadwal' => $jadwal,
            'kelas' => $kelas,
            'id' => $kelas_id,
            'hari' => $hari
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
