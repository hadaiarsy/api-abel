<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Murid;
use App\Kelas;

class MuridPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $murid = Murid::with('kelas')->get();
        return view('admin.murid.index', [
            'murid' => $murid
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Kelas::all();
        return view('admin.murid.create', [
            'kelas' => $kelas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_murid' => 'required',
            'kelas_id' => 'required',
            'alamat_murid' => 'required',
            'no_tlp_murid' => 'required'
        ]);

        Murid::create([
            'is_registered' => "false",
            'nama_murid' => $request->nama_murid,
            'kelas_id' => $request->kelas_id,
            'alamat_murid' => $request->alamat_murid,
            'no_tlp_murid' => $request->no_tlp_murid,
        ]);
        return redirect('/murid')->with('status', 'Data Berhasil masuk.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $murid = Murid::with('kelas')->where('nis', $id)->get();
        $kelas = Kelas::all();
        return view('admin.murid.edit', [
            'murid' => $murid,
            'kelas' => $kelas
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_murid' => 'required',
            'kelas_id' => 'required',
            'alamat_murid' => 'required',
            'no_tlp_murid' => 'required',
        ]);

        Murid::where('nis', $id)->update([
            'nama_murid' => $request->nama_murid,
            'kelas_id' => $request->kelas_id,
            'alamat_murid' => $request->alamat_murid,
            'no_tlp_murid' => $request->no_tlp_murid,
        ]);
        return redirect('/murid')->with('status', 'Data Berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Murid::find($id)->delete();
        return redirect('/murid')->with('status', 'Data berhasil dihapus.');
    }
}
