<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifikasi;
use App\Guru;
use App\Kelas;

class NotifikasiPageController extends Controller
{
    public function index()
    {
        $notif = Notifikasi::with(['guru', 'kelas'])->get();
        return view('admin.notifikasi.index', [
            'notif' => $notif
        ]);
    }

    public function active($id)
    {
        $is_active = Notifikasi::where('id', $id)->get();
        foreach($is_active as $ia) {
            if( $ia->is_active == 0) {
                Notifikasi::find($id)->update([
                    'is_active' => 1
                ]);
                return redirect('/notifikasi')->with('status', 'Notifikasi berhasil diaktifkan.');
            } else {
                Notifikasi::find($id)->update([
                    'is_active' => 0
                ]);
                return redirect('/notifikasi')->with('status', 'Notifikasi berhasil dinonaktifkan.');
            }
        }
    }

    public function show($id)
    {
        $notif = Notifikasi::with(['guru', 'kelas'])->where('id', $id)->get();
        return view('admin.notifikasi.show', [
            'notif' => $notif
        ]);
    }

    public function destroy($id)
    {
        Notifikasi::find($id)->delete();
        return redirect('/notifikasi')->with('status', 'Data berhasil dihapus.');
    }

    public function createNotif()
    {
        $guru = Guru::all();
        $kelas = Kelas::all();
        return view('admin.notifikasi.create', [
            'guru' => $guru,
            'kelas' => $kelas
        ]);
    }

    public function store(Request $request)
    {
        $notif = new Notifikasi;
        $notif->judul_notif = $request->judul_notif;
        $notif->isi_notif = $request->isi_notif;
        $notif->guru_id = $request->guru_id;
        $notif->kelas_id = $request->kelas_id;
        $notif->is_active = $request->is_active;
        $notif->save();
        return redirect('/notifikasi');
    }
}
