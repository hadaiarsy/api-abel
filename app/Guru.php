<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guru extends Model
{
    use SoftDeletes;

    protected $table = "guru";
    protected $primaryKey = "kode_guru";
    protected $fillable = ["nip_guru", "nama_guru", "alamat_guru", "no_tlp_guru", "mapel_id", "password", "phone_id"];
    protected $dates = ["deleted_at"];
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model) {
    		$model->kode_guru = "G-19" . sprintf('%03d', self::generateId());
    	});
    }

    public static function generateId()
    {
        $kodeguru = self::select('kode_guru')->orderBy('kode_guru', 'desc')->take(1)->get();
        if(count($kodeguru) == 0) {
            $kode = "1";
        } else {
            foreach($kodeguru as $kg) {
                $kode = substr($kg->kode_guru, 4, 3) + 1;
            }
        }
        return $kode;
    }

    public function mapel()
    {
        return $this->belongsTo("App\Mapel", "mapel_id", "id");
    }
}
