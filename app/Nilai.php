<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nilai extends Model
{
    use SoftDeletes;

    protected $table = "nilai";
    protected $fillable = ["guru_id", "mapel_id", "nilai_murid", "loop_nilai", "murid_id"];
    protected $dates = ["deleted_at"];

    public function guru()
    {
        return $this->belongsTo("App\Guru", "guru_id", "kode_guru");
    }

    public function mapel()
    {
        return $this->belongsTo("App\Mapel", "mapel_id", "id");
    }

    public function murid()
    {
        return $this->belongsTo("App\Murid", "murid_id", "nisn");
    }
}
