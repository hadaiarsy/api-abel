<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifikasi extends Model
{
    protected $table = "notifikasi";
    protected $fillable = ["judul_notif", "isi_notif", "guru_id", "kelas_id", "is_active"];
    protected $appends = [];

    public function guru()
    {
        return $this->belongsTo("App\Guru", "guru_id", "kode_guru");
    }

    public function kelas()
    {
        return $this->belongsTo("App\Kelas", "kelas_id", "id");
    }

    public function user()
    {
        return $this->belongsTo("App\User", "user_id", "id");
    }
}
