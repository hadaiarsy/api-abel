<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = "jadwal";
    protected $fillabel = ["waktu_awal", "waktu_akhir", "hari", "mapel_id", "ruang", "kelas_id", "guru_id"];

    public function mapel()
    {
        return $this->belongsTo("App\Mapel", "mapel_id", "id");
    }

    public function kelas()
    {
        return $this->belongsTo("App\Kelas", "kelas_id", "id");
    }

    public function guru()
    {
        return $this->belongsTo("App\Guru", "guru_id", "kode_guru");
    }
}
