<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Murid extends Model
{
    use SoftDeletes;

    protected $table = "murid";
    protected $primaryKey = "nis";
    protected $fillable = ["nama_murid", "alamat_murid", "no_tlp_murid", "kelas_id"];
    protected $appends = [
    	"is_registered",
    ];
    protected $dates = ["deleted_at"];
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model) {
    		$model->nis = "171810" . sprintf('%03d', self::generateId());
    	});
    }

    protected function GetStudentStatusAttribute()
    {
    	return $this->attributes['is_registered'] == "true" ? 'Active' : 'Nonactive';
    }

    public static function generateId()
    {
        $nis = self::select('nis')->orderBy('nis', 'desc')->take(1)->get();
        if(count($nis) == 0) {
            $kode = "1";
        } else {
            foreach($nis as $n) {
                $kode = substr($n->nis, 6, 3) + 1;
            }
        }
        return $kode;
    }

    public function kelas()
    {
        return $this->belongsTo("App\Kelas", "kelas_id", "id");
    }
}
